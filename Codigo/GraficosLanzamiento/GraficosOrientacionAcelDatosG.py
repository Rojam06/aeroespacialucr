import sys, serial, argparse
import numpy as np
from time import sleep
from collections import deque
import threading
from time import gmtime, strftime
 
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
 
lectura = "N"
f = open('datosReceptor.csv','w')
# plot class
class AnalogPlot:
  # constr
	
  def __init__(self, maxLen, indice, conversion):
		#global ser
		#ser = serial.Serial('COM8', 9600) 
		# open serial port
		self.ax = deque([0.0]*maxLen)
		self.ay = deque([0.0]*maxLen)
		self.az = deque([0.0]*maxLen)
		self.maxLen = maxLen
		self.indice = indice
		self.lectura = lectura
		self.conversion = conversion
 
  # add to buffer
  def addToBuf(self, buf, val):
      if len(buf) < self.maxLen:
          buf.append(val)
      else:
          buf.pop()
          buf.appendleft(val)
 
  # add data
  def add(self, data):
		self.addToBuf(self.ax, data[self.indice+0]/32768*self.conversion)
		self.addToBuf(self.ay, data[self.indice+1]/32768*self.conversion)
		self.addToBuf(self.az, data[self.indice+2]/32768*self.conversion)
		print "Presion (milibar):\t"+str(data[2])
		print "Altitud (metros):\t"+str(data[3])
		print "Temperatura (C):\t"+str(data[4])
		direccion = "Direccion:\t\t"
		if data[8] < 22.5:
			direccion += "Norte"
		elif data[8] < 67.5:
			direccion += "Noreste"
		elif data[8] < 112.5:
			direccion += "Este"
		elif data[8] < 157.5:
			direccion += "Sureste"
		elif data[8] < 202.5:
			direccion += "Sur"
		elif data[8] < 247.5:
			direccion += "Suroeste"
		elif data[8] < 292.5:
			direccion += "Oeste"
		elif data[8] < 337.5:
			direccion += "Noreste"
		else:
			direccion += "Norte"
		print direccion
 
  # update plot
  def update(self, frameNum, ejeX, ejeY, ejeZ):
		global lectura
		line = lectura
		if len(line) > 0 and line[0] <= "9":
			line = line.rstrip()
			try:
				data = [float(val) for val in line.split(",")]	
				# print data
				if(len(data) == 15): 
					self.add(data)
					ejeX.set_data(range(self.maxLen), self.ax)
					ejeY.set_data(range(self.maxLen), self.ay)
					ejeZ.set_data(range(self.maxLen), self.az)
			except: # catch *all* exceptions
				print "No hay transmision"	
      
		return ejeX, 

				
def leerSerial():
	
	ser = serial.Serial('COM4', 9600) 
	while True:		
	#for x in range(0, 100):
		global lectura
		lectura = ser.readline()##"3,62,832.12,1613.14,32,1000,10000,10000,68,500,10000,-10000,20000,98,10000"#
		if lectura[0] >= "0" and lectura[0] <= "9":
			strTime = strftime("%H,%M,%S", gmtime())
			f.write(strTime+","+lectura)
			
def graficar():
	# plot parameters
	aceleracionPlot = AnalogPlot(10000, 9, 4)
	orientacionPlot = AnalogPlot(10000, 12, 1000)

	print('plotting data...')

	# set up animation
	figA = plt.figure('Aceleracion')
	aceleracion = plt.axes(xlim=(0, 100), ylim=(-4, 4))
	aceleracion.set_title('Aceleracion')
	aceleracion.set_xlabel('Tiempo / s')
	aceleracion.set_ylabel('Aceleracion / g')
	aceleracionX, = aceleracion.plot([], [])
	aceleracionY, = aceleracion.plot([], [])
	aceleracionZ, = aceleracion.plot([], [])
	aceleracion.legend(['Eje X', 'Eje Y','Eje Z'])
	anim = animation.FuncAnimation(figA, aceleracionPlot.update, 
																 fargs=(aceleracionX, aceleracionY, aceleracionZ), 
																 interval=50)																 
	# show plot																	 
	figO = plt.figure('Velocidad de giro')
	orientacion = plt.axes(xlim=(0, 100), ylim=(-1000, 1000))
	orientacion.set_title('Velocidad de giro')
	orientacion.set_xlabel('Tiempo / s')
	orientacion.set_ylabel('Velocidad angular / grados/s')
	orientacionX, = orientacion.plot([], [])
	orientacionY, = orientacion.plot([], [])
	orientacionZ, = orientacion.plot([], [])
	orientacion.legend(['Pitch', 'Roll','Yaw'])
	anim2 = animation.FuncAnimation(figO, orientacionPlot.update, 
																 fargs=(orientacionX, orientacionY, orientacionZ), 
																 interval=50)
																 
	plt.show()

	
def main(): 	 
	t1 = threading.Thread(target=leerSerial)
	t1.start()
	
	t2 = threading.Thread(target=graficar)
	t2.start()

	print('exiting.')
  
 
# call main
if __name__ == '__main__':
  main()