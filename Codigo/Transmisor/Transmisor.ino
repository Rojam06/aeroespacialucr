/*
*    Roberto J. Aguilar
*    rojam06@gmail.com
*    Universidad de Costa Rica


  --- Conexiones del transmisor ---
   1 - GND
   2 - VCC 3.3V !!! NOT 5V
   3 - CE to Arduino pin 9
   4 - CSN to Arduino pin 10
   5 - SCK to Arduino pin 13
   6 - MOSI to Arduino pin 11
   7 - MISO to Arduino pin 12
   8 - UNUSED

   -- Conexiones del barometro

   SDA - A4
   SCL - A5

  --- Conexiones del GPS

  RX - pin 2
  TX - pin 3

*/

//#define DEBUG
#define STORAGE
#define RADIO

/*-----( Bibliotecas )-----*/
#ifdef STORAGE
  #include <SD.h>
#endif
#include <SFE_BMP180.h>
#include <Wire.h>
#include <SPI.h>
#ifdef RADIO
  #include <RF24.h>
#endif
#include <SoftwareSerial.h>
#include "I2Cdev.h"
#include "MPU6050.h"
#include "HMC5883L.h"

/*-----( Declaracion de constantes para numeracion de pines )-----*/
#ifdef RADIO
  #define CE_PIN  9
  #define CSN_PIN 10
#endif
const int chipSelect = 4;
#ifdef RADIO
const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe
#endif

/*-----( Declaracion de objetos )-----*/

SFE_BMP180 bmp;
MPU6050 mpu;
HMC5883L mag;
#ifdef STORAGE
  File archivo;
#endif
char buf[20];
#ifdef RADIO
  RF24 radio(CE_PIN, CSN_PIN);
#endif
/*-----( Declare Variables )-----*/
unsigned int datos[15];
unsigned int secuencia;
unsigned long tiempo;
unsigned long ultimoTiempo;
String hileraDatos = "";

int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t mx, my, mz;

/*-----( Codigo de inicializacion )-----*/
void setup()
{
  Wire.begin();
  Serial.begin(57600);
  secuencia = 0;

  //inicializacion del giroscopio

  mpu.initialize();
  if (!mpu.testConnection())
  {
    Serial.println("MPU6050 - inicializacion fallida\n\n");
    while (1);
  }
  mpu.setFullScaleAccelRange(1);
  mpu.setFullScaleGyroRange(2);

  //inicializacion del sensor de presion
  if (!bmp.begin()) {
    Serial.println("BMP180 - inicializacion fallida\n\n");
    while (1);
  }

  //inicializacion del magnetometro
  mag.initialize();
  if (!mag.testConnection())
  {
    Serial.println("HMC5883L - inicializacion fallida\n\n");
    while (1);
  }
  //inicializacion del transmisor
  #ifdef RADIO
    radio.begin();
    radio.openWritingPipe(pipe);
  #endif

  //inicializacion de la SD
  #ifdef STORAGE
    pinMode(chipSelect, OUTPUT);
    if (!SD.begin(chipSelect)) {
      Serial.println("SD - inicializacion fallida\n\n");
      while (1) ;
    }
  #endif
  ultimoTiempo = 0;
}

void obtenerPresionTemperatura() {
  char status;
  double temperatura, presion;

  status = bmp.startTemperature();
  if (status != 0)
  {
    delay(status);
    status = bmp.getTemperature(temperatura);
    if (status != 0)
    {
      #ifdef DEBUG
            Serial.print("temperature: ");
            Serial.print(temperatura, 5);
            Serial.println(" deg C");
      #endif

      status = bmp.startPressure(3);
      if (status != 0) {
        delay(status);
        status = bmp.getPressure(presion, temperatura);
        if (status != 0) {
    #ifdef DEBUG
              Serial.print("absolute pressure: ");
              Serial.print(presion, 5);
              Serial.println(" mb");
    #endif
            } else {
    #ifdef DEBUG
              Serial.println("Error obteniendo medicion de presion\n");
    #endif
            }
          } else {
    #ifdef DEBUG
            Serial.println("Error iniciando medicion de presion\n");
    #endif
          }
        } else {
    #ifdef DEBUG
          Serial.println("Error obteniendo medicion de temperatura\n");
    #endif
        }
      } else {
    #ifdef DEBUG
        Serial.println("Error iniciando medicion de temperatura\n");
    #endif
  }
  datos[2] = floor(presion);
  datos[3] = (unsigned int)((presion - datos[2]) * pow(10, 4));
  datos[4] = floor(temperatura);
  datos[5] = (unsigned int)((temperatura - datos[4]) * pow(10, 4));


  //Escribir SD
  #ifdef STORAGE
    hileraDatos += String(presion);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(temperatura);
    hileraDatos += ","; //dvision de celdas
  #endif
  
  #ifdef DEBUG
    Serial.println("Termina barometro\n");
  #endif

}

void obtenerDireccion() {
  mag.getHeading(&mx, &my, &mz);

  #ifdef DEBUG
    Serial.print("Magnitud X: ");
    Serial.print(mx);
    Serial.print("  Y: ");
    Serial.print(my);
    Serial.print("  Z: ");
    Serial.println(mz);
  #endif

  #ifdef STORAGE
    hileraDatos += String(mx);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(my);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(mz);
    hileraDatos += ","; //dvision de celdas
  #endif

  datos[6] = mx+32768;
  datos[7] = my+32768;
  datos[8] = mz+32768;
}

void obtenerAceleracionOrientacion() {
  mpu.getMotion6(&ax, &az, &ay, &gx, &gz, &gy);
  ay *= -1;
  gy *= -1;
  #ifdef DEBUG
    Serial.print("Acelerometro:\t");
    Serial.print(ax); Serial.print("\t");
    Serial.print(ay); Serial.print("\t");
    Serial.println(az);
    Serial.print("Giroscopio:\t");
    Serial.print(gx); Serial.print("\t");
    Serial.print(gy); Serial.print("\t");
    Serial.println(gz);
  #endif

  #ifdef STORAGE
    hileraDatos += String(ax);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(ay);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(az);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(gx);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(gy);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(gz);
    hileraDatos += ","; //dvision de celdas
  #endif

  datos[9] =  ax+32768;
  datos[10] = ay+32768;
  datos[11] = az+32768;
  datos[12] = gx+32768;
  datos[13] = gy+32768;
  datos[14] = gz+32768;
}

void loop()
{
  radio.stopListening();
  radio.startWrite( datos, sizeof(datos) );
  tiempo = millis();
  #ifdef DEBUG
    Serial.print("Tiempo: ");
    Serial.println(tiempo);
  #endif
  #ifdef DEBUG
    Serial.print("secuencia: ");
    Serial.println(secuencia);
  #endif

  datos[0] = secuencia;
  datos[1] = tiempo - ultimoTiempo;
  #ifdef STORAGE
    hileraDatos += String(datos[0]);
    hileraDatos += ","; //dvision de celdas
    hileraDatos += String(datos[1]);
    hileraDatos += ","; //dvision de celdas
  #endif
  ultimoTiempo = tiempo;
  obtenerPresionTemperatura();
  obtenerDireccion();
  obtenerAceleracionOrientacion();
  #ifdef GPS
    obtenerPosicion();
  #endif
  radio.startListening();
  #ifdef STORAGE
      archivo = SD.open("datos.csv", FILE_WRITE);
    #ifdef DEBUG
        Serial.println(hileraDatos);
    #endif
        archivo.println(hileraDatos);
        archivo.close();
        hileraDatos = "";
  #endif
  ++secuencia;
  //while (millis() % 80 != 0);
}
