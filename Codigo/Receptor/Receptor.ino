/*
*    Roberto J. Aguilar
*    rojam06@gmail.com
*    Universidad de Costa Rica



--------------------( Conexiones del receptor )------------- 
   1 - GND
   2 - VCC 3.3V !!! NOT 5V
   3 - CE to Arduino pin 9
   4 - CSN to Arduino pin 10
   5 - SCK to Arduino pin 13
   6 - MOSI to Arduino pin 11
   7 - MISO to Amrduino pin 12
   8 - UNUSED
--------------------------------------------------*/

/*-----( Bibliotecas )-----*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <SD.h>



/*-----( Declaracion de constantes para numeracion de pines )-----*/
#define CE_PIN   9
#define CSN_PIN 10

#define RADIO //si el radio no esta difinido, se simula la llegada de paquetes

const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe


/*-----( Declaracion de objetos )-----*/
RF24 radio(CE_PIN, CSN_PIN); // Create a Radio

/*-----( Declare Variables )-----*/
unsigned int datos[15]; 

void setup() 
{
  Serial.begin(9600);
  delay(1000);
  #ifdef RADIO
    radio.begin();
    radio.openReadingPipe(1,pipe);
  #endif
  #ifndef RADIO
    for(int i = 0; i <= 14; ++i)
     datos[i] = i*10;
    datos[2] = 832;
    datos[3] = 4100;
    datos[6] = -300-32600;
    datos[7] = 78-32600;
    datos[8] = 200-32600;
    datos[11] = 21000;
  #endif 
  radio.startListening();
}


void loop() 
{
  #ifdef RADIO
  if ( radio.available() )
  {
    radio.read( datos, sizeof(datos) );
    #endif
    
    Serial.print(datos[0]);
    #ifndef RADIO 
      ++datos[0];
    #endif
    Serial.print(",");
    Serial.print(datos[1]);
    Serial.print(",");
    
    //Imprimir presion
    double presion =  datos[2];
    presion += (float(datos[3])/float(10000));
    Serial.print(presion);
    Serial.print(",");
    
    double altitud = presion / 1013.25;
    altitud = pow(altitud, 0.190284);
    altitud = 1-altitud;
    altitud *= 145366.45;
    altitud *= 0.3048; // a metros   
    
    //Imprimir altitud 
    Serial.print(altitud);
    Serial.print(",");
    
    //Imprimir temperatura     
    Serial.print(datos[4]);
    Serial.print(".");
    if(datos[5]< 10){
      Serial.print ("000");
    }else{
      if(datos[5]< 100){
        Serial.print ("00");  
      }else{
        if(datos[5]< 1000)
          Serial.print ("0"); 
      }
    }
    Serial.print(datos[5]);
    Serial.print(",");
    
    //Imprimir direccion   
    int ejeX = datos[6]-32768;
    int ejeY = datos[7]-32768;
    int ejeZ = datos[8]-32768;
    Serial.print(ejeX);
    Serial.print(",");
    Serial.print(ejeY);
    Serial.print(",");     
    Serial.print(ejeZ);
    Serial.print(",");
    
    float direccion = atan2(-ejeZ, ejeX);
    if(direccion < 0)
      direccion += 2 * M_PI;
     direccion *= 180/M_PI;
    Serial.print(direccion);
    Serial.print(",");   
  
  //Imprimir aceleracion   
  Serial.print(datos[9]-32768);
  Serial.print(",");
  Serial.print(datos[10]-32768);
  Serial.print(",");
  Serial.print(datos[11]-32768);
  Serial.print(",");
  //Imprimir orientacion
     
  Serial.print(datos[12]-32768);
  Serial.print(",");
  Serial.print(datos[13]-32768);
  Serial.print(",");
  Serial.print(datos[14]-32768);
  Serial.print("\n");
  #ifdef RADIO
  }
  else
  {    
      Serial.println("N");
  }
  #endif
}


